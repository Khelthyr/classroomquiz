package com.paci.iut.classroomcommunity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements Serializable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent gIntent = getIntent();
        Bundle bundle = gIntent.getExtras();
        assert bundle != null;
        Personne user = (Personne) bundle.getSerializable("obj");

        TextView affichageNom = findViewById(R.id.presentation);
        affichageNom.setText("Salutation à toi " + user.getNom() + " " + user.getPrenom());
        Log.i("tag","USER NAME : " + user.getNom());

        Button b = findViewById(R.id.buttonQuiz);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(MainActivity.this,QuizActivity.class);
                startActivity(intent);
                //Log.i("tag", "bouton cliqué !!!");
            }
        });
    }
}
