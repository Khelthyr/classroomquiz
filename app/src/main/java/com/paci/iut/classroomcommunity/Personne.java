package com.paci.iut.classroomcommunity;

import java.io.Serializable;

public class Personne implements Serializable {

    private String prenom;
    private String nom;
    private int score;

    public Personne(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
        this.score = 0;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }




}
