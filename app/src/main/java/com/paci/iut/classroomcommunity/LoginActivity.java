package com.paci.iut.classroomcommunity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

public class LoginActivity extends AppCompatActivity implements Serializable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Personne user = new Personne("Pedro", "Dubois");

        Button b = findViewById(R.id.buttonIdentify);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("obj",user);
                intent.putExtras(bundle);

                EditText etIdentif = findViewById(R.id.ETidentifiant);
                EditText etMdp = findViewById(R.id.ETmdp);
                if (etIdentif.getText().toString().equals("a") && etMdp.getText().toString().equals("z")){
                    Toast t = Toast.makeText(v.getContext(),"Vous êtes connecté", Toast.LENGTH_SHORT);
                    t.show();
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else {
                    Log.i("tag","Pseudo ou Mot de passe Incorrect ");
                    Toast t = Toast.makeText(v.getContext(),"Pseudo ou Mot de passe Incorrect", Toast.LENGTH_SHORT);
                    t.show();
                }

            }
        });
    }
}
