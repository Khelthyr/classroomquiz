package com.paci.iut.classroomcommunity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;

public class QuizActivity extends AppCompatActivity {
    CountDownTimer timeCountDown;
    int i = 0;

    Button answer1, answer2, answer3, answer4;
    TextView score, question;
    private Questions mQuestions = new Questions();
    private String mAnswers;
    private int mScore = 0;
    private int mQuestionsLenght = mQuestions.mQuestions.length;

    Random r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        r = new Random();

        answer1 = findViewById(R.id.ButtonResponse1);
        answer2 = findViewById(R.id.ButtonResponse2);
        answer3 = findViewById(R.id.ButtonResponse3);
        answer4 = findViewById(R.id.ButtonResponse4);


        score =  findViewById(R.id.ScoreUser1);
        question = findViewById(R.id.QuestionQuiz);

        updateQuestion(r.nextInt(mQuestionsLenght));

        final ProgressBar timeBar = findViewById(R.id.progressBar);
        timeBar.setProgress(i);
        //CountDownTimer  --> Temps pour répondre à la question.
        timeCountDown = new CountDownTimer(20000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress "+ i + " second " + millisUntilFinished);
                Log.v("Log_tag", "Le score est à : " + mScore);
                i++;
                timeBar.setProgress(i);
                score.setText("" + mScore);
            }

            @Override
            public void onFinish() {
                Log.v("Log_tag", "Timer fini");
                i++;
                timeBar.setProgress(100);
                gameOver();
            }
        };
        timeCountDown.start();

        //Event click bouton 1
        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeCountDown.cancel();
                if(answer1.getText() == mAnswers){
                    i=0;
                    timeCountDown.start();
                    mScore++;
                    //score.setText(mScore);
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameOver();
                }
            }
        });
        //Event click bouton 2
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeCountDown.cancel();
                if(answer2.getText() == mAnswers){
                    i=0;
                    timeCountDown.start();
                    mScore++;
                    updateQuestion(r.nextInt(mQuestionsLenght));

                } else {
                    gameOver();
                }
            }
        });
        //Event click bouton 3
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeCountDown.cancel();
                if(answer3.getText() == mAnswers){
                    i=0;
                    timeCountDown.start();
                    mScore++;
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameOver();
                }
            }
        });
        //Event click bouton 4
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeCountDown.cancel();
                if(answer4.getText() == mAnswers){
                    i=0;
                    timeCountDown.start();
                    mScore++;
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameOver();
                }
            }
        });
    }
    private void updateQuestion(int num){
        question.setText(mQuestions.getQuestion(num));
        answer1.setText(mQuestions.getChoices1(num));
        answer2.setText(mQuestions.getChoices2(num));
        answer3.setText(mQuestions.getChoices3(num));
        answer4.setText(mQuestions.getChoices4(num));

        mAnswers = mQuestions.getCorrectAnswer(num);
    }

    private void gameOver(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(QuizActivity.this);
        alertDialogBuilder
                .setMessage("Fin de la partie ! Votre score est de " + mScore)
                .setCancelable(false)
                .setPositiveButton("Nouvelle Tentative ?",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                startActivity(new Intent(getApplicationContext(), QuizActivity.class));
                            }
                        })
                .setNegativeButton("Quitter",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
