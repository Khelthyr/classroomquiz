package com.paci.iut.classroomcommunity;

public class Questions {

    public String mQuestions[] = {
            "Quelle est la première planète du système solaire ?",
            "Quelle est la deuxième planète du système solaire ?",
            "Quelle est la troisième planète du système solaire ?",
            "Quelle est la quatrième planète du système solaire ?",
            "Quelle est la cinquième planète du système solaire ?",
            "Quelle est la sixième planète du système solaire ?",
            "Quelle est la sexptième planète du système solaire ?",
            "Quelle est la huitème planète du système solaire ?",
            "Quelle est la neuvième planète du système solaire ?"
    };

    public String mChoices[][] = {
            {"Mercure","Vénus","Mars","Jupiter"},
            {"Vénus","Saturne","Mars","Neptune"},
            {"Uranus","Vénus","Terre","Jupiter"},
            {"Mercure","Mars","Terre","Saturne"},
            {"Vénus","Terre","Mars","Jupiter"},
            {"Mercure","Saturne","Mars","Jupiter"},
            {"Mars","Vénus","Pluton","Uranus"},
            {"Mercure","Neptune","Mars","Jupiter"},
            {"Pluton","Terre","Mars","Jupiter"}
    };

    public String mCorrectAnswer[] = {"Mercure", "Vénus", "Terre","Mars","Jupiter", "Saturne", "Uranus", "Neptune", "Pluton"};

    public String getQuestion(int a){
        String question = mQuestions[a];
        return question;
    }

    public String getChoices1(int a){
        String choice = mChoices[a][0];
        return choice;
    }
    public String getChoices2(int a){
        String choice = mChoices[a][1];
        return choice;
    }
    public String getChoices3(int a){
        String choice = mChoices[a][2];
        return choice;
    }
    public String getChoices4(int a){
        String choice = mChoices[a][3];
        return choice;
    }

    public String getCorrectAnswer(int a){
        String answer = mCorrectAnswer[a];
        return answer;
    }
}
