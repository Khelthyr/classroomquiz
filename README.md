# ClassroomQuiz

# Présentation
Ce projet est une application android, il s'agit d'un quiz sur le placement des différentes planets de notre système solaire.
L'objectif est d'obtenir le plus de point possible.

# Requirement
- Android Studio
- Téléphone portable (Android)

# Lancer le projet
- A partir d'android Studio
- Page de connexion 
```
pseudo : a
mdp : z
```

# Affichage des différentes pages

**page de connexion** :
![](./image-readme/ConPage.png)

**page d'accueil** :
![](./image-readme/HomePage.png)

**page de quiz** :
![](./image-readme/QuizPage.png)

**page de résultat** :
![](./image-readme/ResultPage.png)

